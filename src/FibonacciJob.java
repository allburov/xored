import java.util.concurrent.ThreadLocalRandom;

public class FibonacciJob implements Job {
    @Override
    public void work() {
        int randomNum = ThreadLocalRandom.current().nextInt(0, 30);
        int n1 = 0, n2 = 1, n3, i, count = randomNum;
        for (i = 2; i < count; ++i)//loop starts from 2 because 0 and 1 are already printed
        {
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
            System.out.print(" " + n3);
            try {
                //50 should be enough
                Thread.sleep(100);
            } catch (Exception e) {
            }
        }
        System.out.print("\nFINISHED - " + count + "\n");
    }

    public int priority() {
        return 1;
    }
}
