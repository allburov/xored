public interface Job {
    void work();

    int priority();
}
