import java.util.LinkedList;
import java.util.Queue;

class ScheduleExt extends SchedulerImpl {
    void ScheduleMany(Job... jobs) {
        fastSort(jobs);
        for (Job job : jobs) {
            schedule(job);
        }
    }

    static void fastSort(Job[] arr) {
        int n = arr.length;
        Job temp = null;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (arr[j - 1].priority() < arr[j].priority()) {
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }
}

class Main {
    public static void main(String[] args) {
        ScheduleExt scheduler = new ScheduleExt();
        scheduler.ScheduleMany(new FibonacciJob(), new FibonacciJob(), new FibonacciJob());
        scheduler.dispose();

        System.out.println("\nMain thread done");
    }
}