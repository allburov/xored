import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;

public class SchedulerImpl {
    static int TOTAL_WOERKERS_COUNT = 3; //TODO: Nothing works if value more then 1 :-(
    Queue<Job> jobs_queue = new LinkedList<>();
    private ReentrantLock mutex = new ReentrantLock();
    List<Job> jobs = new ArrayList<Job>();
    List<Thread> threads = new ArrayList<Thread>();
    public boolean stopped;

    void dispose() {
        waitAll();
        stopped = true;
    }

    void waitAll() {
        threadSleep(1000);
    }

    public SchedulerImpl() {
        mutex.lock();
        try {
            for (int i = 0; i < TOTAL_WOERKERS_COUNT; i++) {
                Thread thread = new Thread() {
                    public void run() {
                        while (!stopped) {
                            Job j = jobs_queue.poll();
                            if (j != null) {
                                j.work();
                                jobs.add(j);
                            } else {
                                threadSleep(1);
                            }
                        }
                    }
                };
                thread.start();
                threads.add(thread);
            }
        } finally {
            mutex.unlock();
        }

    }

    private void threadSleep(int milliseconds) {
        try {
            //50 should be enough
            Thread.sleep(100);
        } catch (Exception e) {
        }
    }

    public void schedule(Job j) {
        jobs.add(j);
        jobs_queue.add(j);
    }

    public List<Job> Completed() {
        return jobs;
    }
}
