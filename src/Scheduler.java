import java.util.List;

public interface Scheduler {
    List<Job> completed();

    void waitAll();

    void schedule(Job j);

    void scheduleMany(Job... jobs);

    void dispose();
}
